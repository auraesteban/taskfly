var React = require('react');
var ReactFire = require('reactfire');
var Firebase = require('firebase');
var Header = require('./header');
var List = require('./list');
var rootUrl = 'https://taskfly.firebaseio.com/';

var App = React.createClass({
	// mixin - group of methods in an object that is copied on another object
	// ReactFire methods copied on this component
	mixins: [ ReactFire ],
	// Initialize state
	getInitialState: function() {
		return {
			items: {},
			loaded: false
		}
	},
	// Initialize firebase connection
	componentWillMount: function() {
		// bindAsObject method (ReactFire)
		// (Firebase source, where to bind)
		this.fb = new Firebase(rootUrl + 'items/');
		this.bindAsObject(this.fb, 'items');
		// when value is triggered....
		this.fb.on('value', this.handleDataLoaded);
		// this.state.items => {}
	},
	render: function() {
		// Store items from firebase reference
		return <div className="row panel panel-default">
			<div className="col-md-8 col-md-offset-2">
				<h2 className="text-center">
					To-Do List
				</h2>
				<Header itemsStore={this.firebaseRefs.items} />
				<hr />
				<div className={"content " +(this.state.loaded ? 'loaded' : '')}>
					<List items={this.state.items} />
					{this.deleteButton()}
				</div>
				<hr />
			</div>
		</div>
	},
	handleDataLoaded: function() {
		this.setState({loaded: true});
	},
	deleteButton: function() {
		if(!this.state.loaded || !this.state.items) {
			return;
		} else {
			return <div className="text-center clear-complete">
				<hr />
				<button
					type="button"
					onClick={this.onDeleteDoneClick}
					className="btn btn-default">
					Clear Complete
				</button>
			</div>
		}
	},
	onDeleteDoneClick: function() {
		for(var key in this.state.items) {
			if(this.state.items[key].done === true) {
				this.fb.child(key).remove();
			}
		}
	}
});

var element = React.createElement(App, {});
React.render(element, document.querySelector('.container'));
